import React, { Component } from "react";
import Head from "next/head";

class Layout extends Component {
  render() {
    return (
      <div className="App">
        <Head>
          <title>Upgrad-Jeet</title>
          <meta charset="utf-8" />
          <link rel="icon" href="/favicon.ico" />
          <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
          <link rel="manifest" href="%PUBLIC_URL%/site.webmanifest.json" />
          <meta name="theme-color" content="#000000" />
          
        </Head>
        {this.props.children}
      </div>
    );
  }
}

export default Layout;
