import React, { Component } from "react";
import Link from 'next/link';
import Image from 'next/image';



class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
        showDiv: true,
    };
    
  }
  handleClose = (props) => {
    this.setState({ showDiv: false });
  };
  render() {
    return (
      <>
        <div className="container" onClose={this.handleClose}>

          <div
            className="container header"
          >
            India's first ever UGC recognized online BBA, BCA, MBA degree from
            Chandigarh University.&nbsp;&nbsp;
            <Link href="/" >
              <a className="btn btn-sm mt-2 mb-2">KNOW MORE</a>
            </Link>
            <button
              onClick={this.props.onClose}
              type="button"
              className="close"
              aria-label="Close"
            >
              <span  aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
        <nav className="navbar navbar-expand-lg navbar-light">
          <div className="container navbar">
            <Link href="/">
              <a className="navbar-brand"><Image
                src="/images/up_logo.png"
                width={100}
                height={50}
                alt="logo"
                className="d-inline block"
              /></a>
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#Navbar"
              aria-controls="Navbar"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="Navbar">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link  href="#program">
                    <a className="nav-link">MY PROGRAM</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link  href="#ask">
                    <a className="nav-link">ASK DOUBT</a>
                  </Link>
                </li>
              </ul>
              <span className="navbar-nav">
                <li className="none">
                  <Image
                    src="/images/cart.png"
                    alt="img"
                    width={30}
                    height={30}
                    className="d-inline block nav"
                  />
                </li>
                <li className="none">
                  <Image
                    src="/images/bell.png"
                    alt="img"
                    width={30}
                    height={30}
                    className="d-inline block nav"
                  />
                </li>
                <li className="none dropdown">
                  <Image src="/images/ME.jpg" alt="img" className="navround" width={25} height={25}/>
                  <Link
                    href="/"
                  >
                    <a className="d-inline block dropdown-toggle"  id="dropdownMenu"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">Jahnabi</a>
                  </Link>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenu">
                    <Link href="/">
                      <a className="dropdown-item">Action</a>
                    </Link>
                    <Link href="/">
                      <a className="dropdown-item">Another action</a>
                    </Link>
                    <Link href="/">
                      <a className="dropdown-item">Something else here</a>
                    </Link>
                  </div>
                </li>
              </span>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default Navbar;
