import React, { Component } from "react";
import Image from 'next/image'



class Footer extends Component {
  render() {
    return (
      <div className="container footer">
        <div className="row footer">
          <div className="col-lg-3 col-12 text-center">
            <Image src="/images/upGradJeet-logo1.png" alt="logo" height={50} width={100} />
            <p className="pt-3"><Image src="/images/fb.png" className="img" width={15} height={15} />
              <Image src="/images/ins.png" className="img" width={15} height={15} alt="icon"/>
              <Image src="/images/tw.png" className="img" width={15} height={15} alt="icon"/>
              <Image src="/images/pin.png" className="img" width={15} height={15} alt="icon"/></p>
          </div>
          <div className="col-lg-2 col-sm-6 col-12 ">
            <h6>Lorem Ipsum Dolor</h6>
            <p>
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
            </p>
          </div>
          <div className="col-lg-2 col-sm-6 col-12 ">
            <h6>Lorem Ipsum Dolor</h6>
            <p>
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
              <br />
              Lorem Ipsum Dolor
            </p>
          </div>
          <div className="col-lg-2 col-sm-6 col-12 ">
            <h6>UPGRAD JEET</h6>
            <p>
              SERVICES
              <br /> PORTFOLIO
              <br /> PRICING
              <br /> TESTIMONIALS
              <br /> TEAM BLOG
              <br /> CAREER
            </p>
          </div>
          <div className="col-lg-3 col-sm-6 col-12 ">
            <h6>Contact Us</h6>
            <p>
              THE GATE ACADEMY (HO) Vivekananda Nagar #3, 11th A CrossRd, near
              Jockey Factory, Bengaluru, Karnataka 560068, INDIA
            </p>
            <p><Image src="/images/p.png" className="img" width={15} height={15} />
                +918040611000</p>
            <p><Image src="/images/m.png" className="img" width={15} height={15} />
                info@gateacademy.com</p>
          </div>
        </div>
        <hr className="new"></hr>
        <div className="col-12 text-center">
          <p>Copyright &#169; 2018 thegateacademy</p>
        </div>
      </div>
    );
  }
}

export default Footer;
