import TestingStyles from '../styles/Testing.module.css';
const Testing = () => {
    return (
        <div className={TestingStyles.testingStyle + " "+ TestingStyles.xyz}>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio aspernatur repellat consequatur cumque doloremque eveniet eaque ipsum et ad assumenda. Non placeat labore, est corrupti laudantium aperiam impedit consequatur a!</p>
            <div className={TestingStyles.second}></div>
            sample
        </div>
    );
}

export default Testing;


