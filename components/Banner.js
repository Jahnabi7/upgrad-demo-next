import React, { Component } from "react";
import Link from 'next/link';
import Image from 'next/image';



class Banner extends Component {
  render() {
    return (
      <>
        <div className="container banner">
          <div className="card text-white">
            <Image src="/images/bg.jpg" className="card-img" alt="..." width={700} height={700}/>
            <div className="card-img-overlay">
              <h2 className="card-title">
                Lorem ipsum dolor sit
                <br />
                amet, consectetur elit
              </h2>
              <p className="card-text">
                Lorem ipsum dolor sit amet, consectetur adipisicing
                <br />
                elit, sed do eiusmod tempor incididunt
              </p>
              <button type="button" className="btn">
                Explore our courses &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; →
              </button>
              <p className="card-text pt-3">Lorem ipsum dolor sit</p>
            </div>
          </div>
          <div className="container links">
            <div className="links">
            
              <Link href="/">
                <a className="one">Features</a>
              </Link>
              <span className="vl"></span>
              
              <Link href="/">
              <a className="one">
                Knowledge Center
              </a>
              </Link>
              <span className="vl"></span>
              
              <Link href="/">
               <a className="one">Resources</a>
              </Link>
              <span className="vl"></span>
              
              <Link href="/">
                <a className="one">Blogs</a>
              </Link>
              <span className="vl"></span>
              
              <Link href="/">
                <a className="one">2020 Answer Key</a>
              </Link>
            </div>
         
          </div>
        </div>
      </>
    );
  }
}

export default Banner;
