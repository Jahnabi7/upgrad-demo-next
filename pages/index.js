import Layout from '../components/Layout.js';
import Navbar from "../components/Navbar";
import Banner from "../components/Banner";
import Courses from "../components/Courses";
import Testimonials from "../components/Testimonials";
import Footer from "../components/Footer";


function Home() {
  return (
    <Layout>
      <Navbar/>
      <Banner/>
      <Courses/>
      <Testimonials/>
      <Footer/>
    </Layout>
  );
}

export default Home;



